$(function(){
  $('html, body').animate({
    scrollTop: 0
  }, 100);

  $('.topnav a').on("click", function() {
    if($(this).hasClass('disabled')) return;
    $('.topnav a.active').removeClass('active');
    $(this).addClass('active');

    $('html, body').animate({
      scrollTop: $(".news").offset().top + 150
    }, 1000);
  });

  $('.arrow-down').on("click", function() {
    $('html, body').animate({
      scrollTop: $(".about").offset().top - 50
    }, 1000);
    $('.topnav').show();
    $(this).hide();
  });

  $('#home').on("click", function() {
    $('html, body').animate({
      scrollTop: 0
    }, 100);


    $('.topnav').hide();
    $('.arrow-down').show();

  });
});
