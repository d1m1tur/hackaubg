$(function(){
  var activities = [];

  $.ajax({
        type: "POST",
        url: "http://localhost/hackaubg/assets/php/getactivity.php", //Relative or absolute path to response.php file
        success: function(data) {
          var data_array = data.split("&");
          data_array.pop();
          data_array.forEach(function(element) {
              var a = element.split("^");
              var activity = [];
              activity['id'] = a[0];
              activity['title'] = a[1];
              activity['descr'] = a[2];
              activity['created_by'] = a[3];
              activity['images'] = a[4];
              activity['upvotes'] = a[5];
              activity['downvotes'] = a[6];
              activity['type'] = a[7];
              activity['location'] = a[8];
              activity['users'] = a[9];
              activity['link'] = a[10];
              activity['importance'] = a[11];
              activity['scale'] = a[12];
              activity['status'] = a[13];

              activities.push(activity);

              for (var i = 0; i < 10; i++) {
                createActivity(activity);
              }
          });
          window.activities = activities;
          events();
        }
      });

  function createActivity(activity) {
      var el = $($(".activity")[0]).clone();
      el.find('.thumbnail').find('img').attr('src', "data:image/jpeg;base64," + activity['images']);
      el.find('.atitle')[0].innerHTML = activity['title'];
      el.find('.descr')[0].innerHTML = activity['descr'];
      el.find('i.like').find('.number')[0].innerHTML = "&nbsp;" + activity['upvotes'];
      el.find('i.like').data('number', parseInt(activity['upvotes']));
      el.find('i.dislike').find('.number')[0].innerHTML = "&nbsp;" + activity['downvotes'];
      el.find('i.dislike').data('number', parseInt(activity['downvotes']));
      el.find('.location')[0].innerHTML = activity['location'];
      el.show();

      el.clone().appendTo( ".main" );
  }

  function events() {
    $('.main .activity .rate i').on('click', function(){

      var number = $(this).data("number");
      var active = $(this).hasClass('active');

      if($(this).parent().find('.active').find('.number')[0] && !active) {
        $(this).parent().find('.active').find('.number')[0].innerHTML = "&nbsp" + (parseInt($(this).parent().find('.active').data('number')) - 1);
        $(this).parent().find('.active').data('number', parseInt($(this).parent().find('.active').data('number')) - 1);
      }
      $(this).parent().find('.active').removeClass('active');

      if(!active) {
        var newNumber = parseInt(number) + 1;
        $(this).addClass('active');
        $(this).data("number", parseInt($(this).data("number")) + 1);
        $(this).find('.number')[0].innerHTML = "&nbsp" + newNumber;

      } else {
        var newNumber = parseInt(number) - 1;
        $(this).removeClass('active');
        $(this).data("number", parseInt($(this).data("number")) - 1);
        $(this).find('.number')[0].innerHTML = "&nbsp" + newNumber;
      }


    });
  }
});
