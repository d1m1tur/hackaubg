<?php

session_start();

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hackaubg";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM users;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	// output data of each row
	while($row = $result->fetch_assoc()) {
		echo $row['id'] . "^" . $row['name'] . "^" . $row['completed_tasks'] . "^" . $row['tasks'] . "^" . $row['experience'] . "^" . $row['password'] . "^" . $row['email'] . "^" . $row['picture'] . "^" . $row['username'];
		echo "&";
	}
}

$conn->close();

?>